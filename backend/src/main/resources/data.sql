INSERT INTO TEA(name, composition, indications, administration , price , quantity, madeBy ) VALUES ('Digestiv', 'Fructe de coriandru 15%, fructe de anason 15%, roinita 15%, menta 15%, fructe de chimion 10%, salvie 10%, rizomi de ghimbir 5%, fructe de cardamom 5%, catusnica 5%, rostopasca 5%',
'sustine o digestie sanatoasa;contribuie la inlaturarea senzatiei de greata;reduce disconfortul abdominal;favorizeaza eliminarea gazelor intestinale','Se beau 3-4 cani cu ceai pe zi la o jumatate de ora dupa masa, sau la nevoie;Durata curei este de 6-8 saptamani', 5.23, 10, 'Fares');

INSERT INTO SYMPTOM(name) VALUES ('greata');
INSERT INTO SYMPTOM(name) VALUES ('gaze intestinale');

INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (1,1);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (1,2);

INSERT INTO TEA(name, composition, indications, administration , price , quantity, madeBy) VALUES ('Noapte Buna', 'Sunatoare 15%, roinita 15%, radacina de valeriana 15%, conuri de hamei 5%, sulfina 5%, maghiran 5%, flori de tei 5%, passiflora 5%, flori de lavanda',
'ajuta la instalarea unui somn odihnitor si o buna calitate  acestuia, astfel contribuie la un tonus ridicat in ziua urmatoare; sustine relaxarea psihica si echilibrarea balantei emotionale; ajuta la reducerea starilor de agitatie si nervozitate si a senzatiei de oboseala persistenta',
'Se beau 1-2 cani seara, sau la nevoie.Durata curei este de 3-4 saptamani', 5.23, 13,'Fares');

INSERT INTO SYMPTOM (name) VALUES ('agitatie');
INSERT INTO SYMPTOM (name) VALUES ('nervozitate');
INSERT INTO SYMPTOM (name) VALUES ('oboseala persistenta');
INSERT INTO SYMPTOM (name) VALUES ('relaxare');

INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (2,1);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (2,2);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (2,3);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (2,4);

INSERT INTO TEA(name, composition, indications, administration , price , quantity, madeBy ) VALUES ('Ceai pentru somn','Radacina de valeriana30.98%, flori de musetel 18.59%, flori de tei-argintiu 17.7%, sunatoare 7.97%, roinita 6.19%, cimbrisor 6.19%, frunze de paducel 6.19%,' ||
'radacini de lemn dulce 6.19%', 'sustine o stare de relaxare necesara instalarii unui somn profund si odihnitor', 'Se beau 1-2 cani in cursul serii, inainte de culcare.', 4.50, 12, 'Alevia');

INSERT INTO SYMPTOM(name) VALUES ('relaxare');
INSERT INTO SYMPTOM(name) VALUES ('somn');

INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (3,1);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (3,2);

INSERT INTO TEA(name, composition, indications, administration , price , quantity, madeBy ) VALUES ('Ceai digestiv','Seminte de coriandru 14%, fructe de anason 14%, fructe de chimen13%, frunze de papadie 13%, coada-soricelului 11%,flori de galbenele 11%, cimbru-de-cultura 10%, rizomi ' ||
'de ghimbir 9%, scoarta de scortisoara 3%, radacina de lemn-dulce 2%', 'stimularea digestieia secretiei gastrice;ajuta la eliminarea discomfortului abdominal determinat de gaze intestinale si abdominale, de crampe si de hipersecretia gastrica si biliara;contribuie la intensificarea ' ||
'arderii grasimilor si la reglarea absorbtiei si tranzitului intestinal; detoxifiant la nivelul sistemului digestiv','Se beau 2-3 cani de ceai pe zi; se recomanda cure de 3 saptamani',3.60, 4,'AdNatura');

INSERT INTO SYMPTOM(name) VALUES ('crampe');
INSERT INTO SYMPTOM(name) VALUES ('detoxifiant');
INSERT INTO SYMPTOM(name) VALUES ('hipersecretie gastrica');
INSERT INTO SYMPTOM(name) VALUES ('hipersecretie biliara');
INSERT INTO SYMPTOM(name) VALUES ('disconfort gastric');
INSERT INTO SYMPTOM(name) VALUES ('gaze');

INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,1);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,2);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,3);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,4);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,5);
INSERT INTO TEA_SYMPTOMS(tea_id, symptom_id) VALUES (4,6);

