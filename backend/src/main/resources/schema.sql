DROP TABLE IF EXISTS TEA;
DROP TABLE IF EXISTS TEA_SYMPTOMS;
DROP TABLE IF EXISTS SYMPTOM;

CREATE TABLE TEA (id SERIAL PRIMARY KEY, name VARCHAR(100), composition VARCHAR(900), indications VARCHAR(900),
                  administration VARCHAR(300), price MONEY, quantity INTEGER, madeBy VARCHAR (100));
CREATE TABLE TEA_SYMPTOMS (tea_id INTEGER, symptom_id INTEGER);
CREATE TABLE SYMPTOM(id SERIAL PRIMARY KEY, name VARCHAR(100));
