package com.pharmacy.repository;

import com.pharmacy.entity.Symptom;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Marina on 1/29/2019.
 */
@Repository
public interface SymptomRepository extends JpaRepository<Symptom, Long> {
  List<Symptom> findAll();
  Symptom findSymptomById(Integer id);
  Symptom findSymptomByName(String name);




}
