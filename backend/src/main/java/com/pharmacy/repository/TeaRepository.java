package com.pharmacy.repository;

import com.pharmacy.entity.Tea;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Marina on 1/28/2019.
 */
@Repository
public interface TeaRepository extends JpaRepository<Tea, Long> {

  List<Tea> findAll();

  Tea findTeaById(Integer id);

  Tea findTeaByName(String name);

  Tea save(Tea newTea);
}