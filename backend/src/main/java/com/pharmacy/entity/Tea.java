package com.pharmacy.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by Marina on 1/28/2019.
 */
@Entity
@Table(name = "tea")
public class Tea {

  @Id
  @SequenceGenerator(name = "tea_id_seq", sequenceName = "tea_id_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tea_id_seq")
  @Column(name = "id", updatable = false)
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "composition")
  private String composition;

  @Column(name = "indications")
  private String indications;

  @Column(name = "administration")
  private String administration;

  @Column(name = "price")
  private BigDecimal price;

  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "madeby")
  private String madeBy;

  public Tea(String name, String composition, String indications, String administration,
      BigDecimal price, Integer quantity, String madeBy) {
    this.name = name;
    this.composition = composition;
    this.indications = indications;
    this.administration = administration;
    this.price = price;
    this.quantity = quantity;
    this.madeBy = madeBy;
  }

  public Tea() {

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getComposition() {
    return composition;
  }

  public void setComposition(String composition) {
    this.composition = composition;
  }

  public String getIndications() {
    return indications;
  }

  public void setIndications(String indications) {
    this.indications = indications;
  }

  public String getAdministration() {
    return administration;
  }

  public void setAdministration(String administration) {
    this.administration = administration;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public String getMadeBy() {
    return madeBy;
  }

  public void setMadeBy(String madeBy) {
    this.madeBy = madeBy;
  }

  @Override
  public String toString() {
    return "Tea{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", composition='" + composition + '\'' +
        ", indications='" + indications + '\'' +
        ", administration='" + administration + '\'' +
        ", price=" + price +
        ", quantity=" + quantity +
        ", madeBy='" + madeBy + '\'' +
        '}';
  }
}
