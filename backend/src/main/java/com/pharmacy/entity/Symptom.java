package com.pharmacy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by Marina on 1/29/2019.
 */
@Entity
@Table(name="symptom")
public class Symptom {
   @Id
   @SequenceGenerator(name="symptom_id_seq", sequenceName="symptom_id_seq", allocationSize=1)
   @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="symptom_id_seq")
   @Column(name="id", updatable=false)
    private Integer id;

   @Column(name="name")
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
