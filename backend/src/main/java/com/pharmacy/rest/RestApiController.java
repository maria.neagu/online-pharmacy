package com.pharmacy.rest;

import com.pharmacy.entity.Symptom;
import com.pharmacy.entity.Tea;
import com.pharmacy.repository.SymptomRepository;
import com.pharmacy.repository.TeaRepository;
import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Created by Marina on 1/28/2019.
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RestApiController {

  private final static Logger logger = LoggerFactory.getLogger(RestApiController.class);


  @Autowired
  private TeaRepository teaRepository;

  @Autowired
  private SymptomRepository symptomRepository;


  @GetMapping("/isAlive")
  public String isAlive() {
    return "Service is up and running";
  }

  @GetMapping("/teas")
  public List<Tea> findAll() throws Exception {
    List<Tea> teas = teaRepository.findAll();

    if (teas.size() == 0) {
      logger.error("Call method findAll returned 0 teas");
      throw new Exception("No teas in database");
    } else {
      return teas;

    }
  }

  @GetMapping("/teas/{id}")
  public Tea findTeaById(@PathVariable Integer id) {
    logger.info("Called endpoint findTeaById with id="+id);
    return teaRepository.findTeaById(id);
  }

  @GetMapping("teas/name/{name}")
  public Tea findTeaByName(@PathVariable String name) {
    logger.info("Called endpoint findTeaByName with name="+name);
    return teaRepository.findTeaByName(name);
  }

  @GetMapping("/symptoms")
  public List<Symptom> findAllSymptoms() {
    logger.info("Called endpoint findAllSymptoms");
    return symptomRepository.findAll();

  }

  @GetMapping("/symptoms/{id}")
  public Symptom findSymptomById(@PathVariable Integer id) {
    logger.info("Called endpoint findSymptomById with id="+id);
    return symptomRepository.findSymptomById(id);
  }

  @GetMapping("/symptoms/name/{name}")
  public Symptom findSymptomByName(@PathVariable String name) {
    logger.info("Called endpoint findSymptomByName with name="+name);
    return symptomRepository.findSymptomByName(name);
  }

  @PostMapping("/teas")
  public ResponseEntity<Void> createTea(@RequestBody final Tea newTea) {
    logger.info("Called endpoint create Tea ");
    Tea tea = teaRepository.save(newTea);

    if (tea == null) {
      return ResponseEntity.noContent().build();
    }

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}").buildAndExpand(tea.getId()).toUri();

    return ResponseEntity.created(location).build();
  }

}