package com.pharmacy.repository;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.pharmacy.entity.Symptom;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)

public class SymptomRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private SymptomRepository symptomRepository;


  @Test
  @Description("Should return a list of symptoms")
  public void findAll() {
    List<Symptom> symptoms = symptomRepository.findAll();
    Assert.assertEquals(symptoms.size(), 14);
  }

  @Test
  public void findSymptomById() {
    //act
    Symptom symptom = symptomRepository.findSymptomById(1);
    System.out.println(symptom);
    //assert
    Assert.assertNotNull(symptom);
    Assert.assertEquals(symptom.getId(), Integer.valueOf(1));
    Assert.assertEquals(symptom.getName(), String.valueOf("greata"));

  }

  @Test
  public void findSymptomByName() {
    //act
    Symptom symptom = symptomRepository.findSymptomByName("greata");
    System.out.println(symptom);
    //assert
    Assert.assertNotNull(symptom);
    Assert.assertEquals(symptom.getName(), String.valueOf("greata"));

  }
}