package com.pharmacy.repository;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.pharmacy.entity.Tea;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest// pt a mockui Entity Managerul
@AutoConfigureTestDatabase(replace = NONE)
public class TeaRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private TeaRepository teaRepository;

  @Test
  @Description("Should return a list of teas")
  public void findAll() {
    //act
    List<Tea> teas = teaRepository.findAll();

    //assert
    Assert.assertEquals(teas.size(), 4);
  }

  @Test
  public void findTeaById() {
    //arrange
    //act
    Tea tea = teaRepository.findTeaById(1);
    System.out.print(tea);
    //assert
    //Assert.assertEquals(java.util.Optional.ofNullable(tea.getId()), Optional.of(1));
    Assert.assertNotNull(tea);
    Assert.assertEquals(tea.getId(), Integer.valueOf(1));
    Assert.assertEquals(tea.getName(), "Digestiv");
  }

  @Test
  public void findTeaByName() {
    //arrange
    //act
    Tea tea = teaRepository.findTeaByName("Digestiv");
    System.out.println(tea);
    //assert
    Assert.assertNotNull(tea);
    Assert.assertEquals(tea.getName(), String.valueOf("Digestiv"));
  }

  @Test
  public void save() {
    //arrange
    Tea teaToBeSaved = new Tea("mock_tea_name", "mock_tea_compositio", "mock_tea_composition",
        "mock_tea_administration", new BigDecimal(3), 2, "mock_tea_madeBy");

    //act
    Tea savedTea = teaRepository.save(teaToBeSaved);

    //assert
    Assert.assertNotNull(savedTea);
    Assert.assertEquals(savedTea.getId(), teaToBeSaved.getId());
    Assert.assertEquals(savedTea.getName(), teaToBeSaved.getName());
    Assert.assertEquals(savedTea.getAdministration(), teaToBeSaved.getAdministration());
    Assert.assertEquals(savedTea.getComposition(), teaToBeSaved.getComposition());
    Assert.assertEquals(savedTea.getIndications(), teaToBeSaved.getIndications());
    Assert.assertEquals(savedTea.getPrice(), teaToBeSaved.getPrice());
    Assert.assertEquals(savedTea.getMadeBy(), teaToBeSaved.getMadeBy());
    Assert.assertEquals(savedTea.getQuantity(), teaToBeSaved.getQuantity());
  }
}