package com.pharmacy.rest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pharmacy.entity.Symptom;
import com.pharmacy.entity.Tea;
import com.pharmacy.repository.SymptomRepository;
import com.pharmacy.repository.TeaRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Description;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(RestApiController.class)
public class RestApiControllerTest {

  @MockBean
  TeaRepository teaRepository;
  @MockBean
  SymptomRepository symptomRepository;

  @Autowired
  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void isAlive() throws Exception {
    //arange
    String uri = "/isAlive";

    //act
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
    assertEquals(mvcResult.getResponse().getContentAsString(), "Service is up and running");

  }

  @Test
  public void findAll() throws Exception {
    //arrange
    when(teaRepository.findAll())
        .thenReturn(new ArrayList<Tea>(Arrays.asList(new Tea(), new Tea())));
    String uri = "/teas";

    //act
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
    List<Tea> teasRespose = stringToObject(mvcResult);
    assertEquals(teasRespose.size(), 2);
  }

  @Test(expected = Exception.class)
  @Description("Should throw exception")
  public void findAllShouldThrowException() throws Exception {
    //arrange
    when(teaRepository.findAll()).thenThrow(new Exception("No teas in database"));
    String uri = "/teas";

    //act
    mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
        .andReturn();
  }

  private List<Tea> stringToObject(MvcResult mvcResult) throws IOException {
    List<Tea> teasResponse;
    ObjectMapper objectMapper = new ObjectMapper();
    teasResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Tea>>() {
        });
    return teasResponse;
  }

  protected String mapToJson(Object obj) throws JsonProcessingException {

    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(obj);

  }

  protected Tea mapFronJson(String json, Class<Tea> tClass)
      throws JsonParseException, JsonMappingException, IOException {

    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(json, Tea.class);
  }


  @Test
  @Description("Should return 2 teas")
  public void findTeaById() throws Exception {
    //arrange
    when(teaRepository.findTeaById(2)).thenReturn(new Tea());

    String uri = "/teas/{2}";

    //act
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, 2)
        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);

  }

  @Test
  public void findTeaByName() throws Exception {
    //arrange
    when(teaRepository.findTeaByName("Digestiv")).thenReturn(new Tea());
    String uri = "/teas/name/{name}";

    //act
    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.get(uri, "Digestiv").accept(MediaType.APPLICATION_JSON_VALUE))
        .andReturn();

    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);

  }

  @Test
  public void findAllSymptoms() throws Exception {
    //arrange
    when(symptomRepository.findAll())
        .thenReturn(new ArrayList<Symptom>(Arrays.asList(new Symptom(), new Symptom())));
    String uri = "/symptoms";

    //act
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
  }

  @Test
  public void findSymptomById() throws Exception {
    //arrange
    when(symptomRepository.findSymptomById(3)).thenReturn(new Symptom());
    String uri = "/symptoms/{id}";

    //act
    MvcResult mvcResult = mvc
        .perform(MockMvcRequestBuilders.get(uri, 3).accept(MediaType.APPLICATION_JSON_VALUE))
        .andReturn();
    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);

  }


  @Test
  public void findSymptomByName() throws Exception {
    //arrange
    when(symptomRepository.findSymptomByName("greata")).thenReturn(new Symptom());
    String uri = "/symptoms/name/{name}";

    //act
    MvcResult mvcResult = mvc.perform(
        MockMvcRequestBuilders.get(uri, "greata").accept(MediaType.APPLICATION_JSON_VALUE))
        .andReturn();

    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);

  }

  @Test
  public void createTea() throws Exception {
    //arrange
    Tea newTea = new Tea();
    newTea.setName("Ceai de galbenele");
    newTea.setId(15);
    when(teaRepository.save(any(Tea.class))).thenReturn(newTea);
    String uri = "/teas";
    String inputJson = mapToJson(newTea);

    //act
    MvcResult mvcResult = mvc
        .perform(MockMvcRequestBuilders.post(uri)
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
        .andReturn();

    //assert
    int status = mvcResult.getResponse().getStatus();
    assertEquals(201, status);
  }

  protected String mapToJson(Tea newTea) throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(newTea);

  }
}
