import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Tea} from '../model/Tea';
import {map} from 'rxjs/operators';

@Injectable({providedIn: "root"})
export class DataService {
  private baseUrl: string = 'http://localhost:8080/myapp';

  constructor(private http: HttpClient) {
  }

  getTeas(): Observable<Tea[]> {
    return this.http.get(this.baseUrl + '/teas')
    .pipe(map((response: Tea[]) => this.mapTeaResponse(response)));
  }

  private mapTeaResponse(response: Tea[]): Tea[] {
    return response;
  }
}
