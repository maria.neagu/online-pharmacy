import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig, MatTableDataSource} from '@angular/material';
import {Tea} from '../../model/Tea';
import {DataService} from '../../service/data.service';
import {TeaDetailDialogComponent} from '../tea-detail-dialog/tea-detail-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'composition', 'indications', 'administration', 'price', 'quantity', 'madeBy'];
  dataSource = new MatTableDataSource<Tea>();


  constructor(private dataService: DataService, private dialog: MatDialog){}

  ngOnInit() {
    this.dataService.getTeas().subscribe((response: Tea[]) => {
        console.log(response);
        this.dataSource = new MatTableDataSource<Tea>(response);

      }
    );

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toString();

  }
  openDetails(row:Tea)
{
  console.log(row);
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.data = row;

  this.dialog.open(TeaDetailDialogComponent, dialogConfig);

}
}
