import {Component, Inject, OnInit} from '@angular/core';
import {Tea} from '../../model/Tea';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';



@Component({
  selector: 'app-tea-detail-dialog',
  templateUrl: './tea-detail-dialog.component.html',
  styleUrls: ['./tea-detail-dialog.component.less']
})
export class TeaDetailDialogComponent implements OnInit {

  private teaDetail: Tea;
  private teaTitle: string;

  // noinspection JSAnnotator
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TeaDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.teaDetail = data;
    this.teaTitle = 'Ceaiul' + this.teaDetail.name;
  }
  ngOnInit(){
  }
   ok(){
      this.dialogRef.close();
  }
   }
