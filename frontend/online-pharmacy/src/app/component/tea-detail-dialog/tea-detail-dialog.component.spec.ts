import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeaDetailDialogComponent } from './tea-detail-dialog.component';

describe('TeaDetailDialogComponent', () => {
  let component: TeaDetailDialogComponent;
  let fixture: ComponentFixture<TeaDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeaDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeaDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
