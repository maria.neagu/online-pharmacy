import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './component/header/header.component';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {MatTableModule} from "@angular/material";
import {HttpClientModule} from "@angular/common/http";
import {FooterComponent} from './component/footer/footer.component';
import {TeaDetailDialogComponent} from './component/tea-detail-dialog/tea-detail-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    FooterComponent,
    TeaDetailDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    NoopAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
   entryComponents:[TeaDetailDialogComponent]

})
export class AppModule {
}
