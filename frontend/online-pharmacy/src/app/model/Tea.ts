export interface Tea {
  id: string;
  name: string;
  composition: string;
  indications: string;
  administration: string;
  price: string;
  quantity: string;
  madeby: string;
}
